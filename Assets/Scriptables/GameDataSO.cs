﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "ScriptableObjects/GameData", order = 1)]
public class GameDataSO : ScriptableObject
{
    public float maxVelocity;
    public float minVelocity;

    public MovementData acceleration, deceleration;

    [System.Serializable]
    public class MovementData
    {
        public float fullSeconds;
        public AnimationCurve curve;
    }

    [Range(0, 100)] [Header("States")]
    public int deceleratePercent = 33;
    public float decelerationSeconds;
    public int transitionSeconds;
}