﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SlidesManager : MonoBehaviour
{
    public LoopType type;

    public enum LoopType
    {
        Manual = 0,
        Auto = 1
    }

    public Sprite[] sprites;

    public float waitSeconds;

    public RectTransform current, target;
    public Image myImage;
    public CanvasGroup cg;

    public string nextSceneName;

    public float secondsPerSlide = 4.5f;

    private float fullSeconds = 26;

    public GameObject feedback;

    public IEnumerator Start()
    {
        yield return new WaitForSeconds(waitSeconds);

        float waitTime = secondsPerSlide; //fullSeconds / sprites.Length - 2;

        for (int i = 0; i < sprites.Length; i++)
        {
            yield return SwapImage(sprites[i], i == 0);

            if (type == LoopType.Auto)
            {
                yield return new WaitForSeconds(waitTime);
            }
            else
            {
                if (feedback != null)
                {
                    feedback.SetActive(true);
                }

                yield return new WaitUntil(()=> Input.GetKeyDown(KeyCode.Space));

                if (feedback != null)
                {
                    feedback.SetActive(false);
                }
            }
        }

        Fader.Instance.StartFade(() => { SceneManager.LoadScene(nextSceneName); });
    }

    // 2 seconds animation
    private IEnumerator SwapImage(Sprite newSprite, bool skipFirst)
    {
        float step = 0;

        if (!skipFirst)
        {
            while (step < 1)
            {
                step += Time.deltaTime / .8f;
                myImage.transform.position = Vector3.Lerp(current.position, target.position, step);
                yield return null;
            }
        }

        step = 0;


        myImage.sprite = newSprite;
        cg.alpha = 0;
        myImage.rectTransform.position = current.position;

        Vector3 startScale = new Vector3(1.6f, 1.6f, 1.6f);
        float targetRotation = Random.Range(-5f, 5f);

        myImage.rectTransform.localScale = startScale;

        while (step < 1)
        {
            step += Time.deltaTime / 0.8f;

            cg.alpha = Mathf.Lerp(0, 2, step);
            myImage.rectTransform.localScale = Vector3.Lerp(startScale, Vector3.one, step);
            myImage.rectTransform.localEulerAngles = new Vector3(myImage.rectTransform.localEulerAngles.x, myImage.rectTransform.localEulerAngles.y, Mathf.Lerp(0, targetRotation, step));

            yield return null;
        }
    }
}