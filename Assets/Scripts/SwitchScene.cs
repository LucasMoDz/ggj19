﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScene : MonoBehaviour
{
    public TriggerType type;

    public string sceneName;
    public float waitSeconds;

    public bool shouldFade;

    private bool changed;
    
    public enum TriggerType
    {
        Auto = 0,
        Space = 1
    }

    IEnumerator Start()
    {
        if (type == TriggerType.Space)
            yield break;

        yield return new WaitForSeconds(waitSeconds);

        ChangeScene();
    }

    private void Update()
    {
        if (type == TriggerType.Auto)
            return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            ChangeScene();
        }
    }

    private void ChangeScene()
    {
        if (changed)
            return;

        changed = true;

        if (shouldFade)
        {
            Fader.Instance.StartFade(() => SceneManager.LoadScene(sceneName));
        }
        else
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}