﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
	private bool hit = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!hit && collision.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.PlayerHitObstacle();
            SoundManager.Instance.PlayEffect("cuteImpact");

            hit = true;
        }
    }
}