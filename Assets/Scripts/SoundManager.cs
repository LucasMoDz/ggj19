﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    public AudioSource musicSource, effectSource;

    public List<AudioClip> audioClips, effectClips;

    private Coroutine switchMusicCoroutine;

    public void PlayEffect(string clipName, float? volume = null)
    {
        AudioClip clip = effectClips.Find(x => x.name == clipName);

        if (clip == null)
        {
            Debug.LogError("Effect " + clipName + " not found!\n");
            return;
        }

        if (volume == null)
            effectSource.PlayOneShot(clip);
        else
            effectSource.PlayOneShot(clip, volume.Value);
    }

    public void SetMusicLoopState(bool isInLoop)
    {
        musicSource.loop = isInLoop;
    }

    public void PlayMusic(string musicName, float seconds = 1f)
    {
        if (musicSource.clip != null && musicSource.clip.name == musicName)
        {
            Debug.LogWarning("Why do you call the same clip?\n");
            return;
        }

        if (switchMusicCoroutine != null)
        {
            return;
        }

        AudioClip music = audioClips.Find(x => x.name == musicName);

        if (music == null)
        {
            Debug.LogError("Effect " + musicName + " not found!\n");
            return;
        }

        switchMusicCoroutine = StartCoroutine(PlayMusicCO(music, seconds));
    }

    private IEnumerator PlayMusicCO(AudioClip clip, float _seconds)
    {
        float step = 1;

        if (musicSource.clip != null)
        {
            step = 0;

            while (step < 1)
            {
                step += Time.deltaTime / (_seconds * .75f);
                musicSource.volume = Mathf.Lerp(1, 0, step);
                yield return null;
            }
        }
        
        musicSource.volume = 0;
        musicSource.clip = clip;
        musicSource.Play();

        while (step > 0)
        {
            step -= Time.deltaTime / _seconds;
            musicSource.volume = Mathf.Lerp(1, 0, step);
            yield return null;
        }

        switchMusicCoroutine = null;
    }
}