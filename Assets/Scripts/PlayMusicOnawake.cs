﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMusicOnawake : MonoBehaviour
{
    public float waitSeconds = .5f;
    public string id;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(waitSeconds);
        SoundManager.Instance.PlayMusic(id);
    }
}
