﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherManager : Singleton<WeatherManager>
{
    public Material floorMaterial;
    public Color floorStartColor;
    public Color floorTargetColor;

    public Material cloudMaterial;
    public Color cloudStartColor;
    public Color cloudTargetColor;

    public SpriteRenderer bgImage;
    public Color bgTargetColor;

    public SpriteRenderer starsImage;

    public SpriteRenderer playerImage;
    public Color playerTargetColor;

    public Coroutine BecomeNight()
    {
        return StartCoroutine(BecomeNightCO());
    }

    public void Awake() {
        floorMaterial.color = floorStartColor;
        cloudMaterial.color = cloudStartColor;
    }

    private IEnumerator BecomeNightCO()
    {
        float step = 0;

        Color startMaterialColor = floorMaterial.color;
        Color startCloudMaterialColor = cloudMaterial.color;
        Color startBgColor = bgImage.color;
        Color startPlayerColor = playerImage.color;

        SoundManager.Instance.PlayMusic("sadMusic");

        while (step < 1)
        {
            floorMaterial.color = Color.Lerp(startMaterialColor, floorTargetColor, step);
            cloudMaterial.color = Color.Lerp(startCloudMaterialColor, cloudTargetColor, step);
            bgImage.color = Color.Lerp(startBgColor, bgTargetColor, step);
            playerImage.color = Color.Lerp(startPlayerColor, playerTargetColor, step);

            Color starsColor = starsImage.color;
            starsColor.a = Mathf.Lerp(0, 1, step);
            starsImage.color = starsColor;

            step += Time.deltaTime / GameManager.Instance.gameData.transitionSeconds;
            yield return null;
        }

        floorMaterial.color = floorTargetColor;
        cloudMaterial.color = cloudTargetColor;
        bgImage.color = bgTargetColor;
        playerImage.color = playerTargetColor;

        Debug.Log("It is night!\n");
    }

    public void StopLogic()
    {
        StopAllCoroutines();
    }
}