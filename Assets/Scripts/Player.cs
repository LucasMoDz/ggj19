﻿using System.Collections;
using UnityEngine;

public class Player : Singleton<Player>
{
    public float force = 18;
    public bool singleJump = true;

    private bool canWoof = false;
    private bool canJump = true;

    private bool isJumping;
    private bool canStart = false;

    private Rigidbody2D rb;

    private float maxWaitSeconds = 1.8f;
    private float minWaitSeconds = 0.4f;

    private float maxAnimationMult = 1.5f;
    private float minAnimationMult = 0.0f;

    private void Awake()
    {
        rb = this.GetComponent<Rigidbody2D>();

        if (rb == null)
        {
            Debug.LogError("Rb is null\n");
        }

        AvoidJump();
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.05f);
        canStart = true;
    }

    private Coroutine soundCoroutine;

    public void StartPlaySounds()
    {
        if (soundCoroutine != null)
            return;

        soundCoroutine = StartCoroutine(PlaySounds());
    }

    public void StopPlaySounds()
    {
        if (soundCoroutine == null)
        {
            return;
        }

        StopCoroutine(soundCoroutine);
        soundCoroutine = null;
    }

    private IEnumerator PlaySounds()
    {
        while (true)
        {
            SoundManager.Instance.PlayEffect("cuteWalk");

            float maxVelocity = GameManager.Instance.gameData.maxVelocity;
            float currentVelocity = BackgroundManager.Instance.GetCurrentVelocity();

            float t = currentVelocity / maxVelocity;
            float seconds = Mathf.Lerp(maxWaitSeconds, minWaitSeconds, t);

            Debug.Log("Wait seconds: " + seconds + "\n");

            yield return new WaitForSeconds(seconds);

            yield return null;
        }
    }

    public void SetIdleState()
    {
        GetComponentInChildren<Animator>().SetTrigger("Idle");
    }

    public void Woof() {
        GetComponentInChildren<Animator>().SetTrigger("Woof");
    }

    public void AllowJump()
    {
        canJump = true;
    }

    public void AvoidJump()
    {
        canJump = false;
    }

    public void AvoidJumpAndAllowWoof()
    {
        AvoidJump();
        canWoof = true;
    }

    private void Update()
    {

        if (!canStart)
            return;

        // set animation speed
        float maxVelocity = GameManager.Instance.gameData.maxVelocity;
        float currentVelocity = BackgroundManager.Instance.GetCurrentVelocity();

        float t = currentVelocity / maxVelocity;
        float speedMult = Mathf.Lerp(minAnimationMult, maxAnimationMult, t);

        GetComponentInChildren<Animator>().SetFloat("WalkSpeed", speedMult);

        if (Input.GetKeyDown(KeyCode.LeftArrow) && !isJumping) {
            GameManager.Instance.StopGameplay();
        }

        if (!canJump)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (canJump)
            {
                if (singleJump && isJumping)
                {
                    return;
                }

                rb.AddForce(new Vector2(0, force), ForceMode2D.Impulse);
            }
            else if (canWoof)
            {
                Woof();
                SoundManager.Instance.PlayEffect("sadBau");
            }
            else
            {
                Debug.LogError("Impossible state!! ERROR\n");
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.gameObject.CompareTag("Floor"))
        {
            return;
        }

        isJumping = false;
        GetComponentInChildren<Animator>().SetTrigger("JumpEnd");
        SoundManager.Instance.PlayEffect("cuteImpact");
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (!other.gameObject.CompareTag("Floor"))
        {
            return;
        }

        isJumping = true;
        GetComponentInChildren<Animator>().SetTrigger("JumpStart");
        SoundManager.Instance.PlayEffect("cuteJump");
    }
}