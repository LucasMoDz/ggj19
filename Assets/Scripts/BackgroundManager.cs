﻿using System.Collections;
using UnityEngine;

public class BackgroundManager : Singleton<BackgroundManager>
{
    public Transform background;
    public Transform clouds;

    private GameDataSO data;
    private float currentVelocity;
    private bool canMove = false;

    private Coroutine accelerationCoroutine, decelerationCoroutine;

    private void Awake()
    {
        data = GameManager.Instance.gameData;
    }

    public float GetCurrentVelocity()
    {
        return currentVelocity;
    }

    public void StartMovement()
    {
        canMove = true;
        StartCoroutine(AccelerateCO(true));
    }

    public void Accelerate(bool isFirstTime)
    {
        if (accelerationCoroutine != null)
        {
            return;
        }

        accelerationCoroutine = StartCoroutine(AccelerateCO(isFirstTime));
    }

    private IEnumerator AccelerateCO(bool isFirstTime)
    {
        if (!isFirstTime)
        {
            yield return new WaitForSeconds(0.15f);
        }

        float step = 0;

        float startValue = currentVelocity;
        float targetValue = data.maxVelocity;
        float seconds = data.acceleration.fullSeconds * ((targetValue - startValue) / targetValue);

        Debug.Log("Acceleration from " + startValue + " to " + targetValue + " in " + seconds + " seconds.\n");

        while (step < 1f)
        {
            currentVelocity = Mathf.Lerp(startValue, targetValue, data.acceleration.curve.Evaluate(step));
            step += Time.deltaTime / seconds;
            yield return null;
        }

        currentVelocity = targetValue;

        accelerationCoroutine = null;
    }

    public Coroutine Decelerate(bool end = false)
    {
        if (accelerationCoroutine != null)
        {
            StopCoroutine(accelerationCoroutine);
            accelerationCoroutine = null;
        }

        if (decelerationCoroutine != null)
        {
            StopCoroutine(decelerationCoroutine);
            decelerationCoroutine = null;
        }

        return decelerationCoroutine = StartCoroutine(DecelerateCO(end));
    }

    private IEnumerator DecelerateCO(bool end)
    { 
        float step = 0;

        float startValue = currentVelocity;
        float targetValue = Mathf.Clamp(currentVelocity - currentVelocity / 4f, data.minVelocity, data.maxVelocity);
        float seconds = data.deceleration.fullSeconds * (Mathf.Abs(targetValue - startValue) / targetValue);

        if (end)
        {
            targetValue = 0;
            seconds = 1f;
        }

        Debug.Log("Deceleration from " + startValue + " to " + targetValue + " in " + seconds + " seconds.\n");

        while (step < 1)
        {
            currentVelocity = Mathf.Lerp(startValue, targetValue, data.deceleration.curve.Evaluate(step));
            step += Time.deltaTime / seconds;
            yield return null;
        }

        currentVelocity = targetValue;

        decelerationCoroutine = null;

        if (!end)
            Accelerate(false);
        else
            Player.Instance.GetComponentInChildren<SpriteRenderer>().flipX = true;

    }

    private void Update()
    {
        if (!canMove)
            return;

        MoveBackground();
    }

    private void MoveBackground()
    {
        Vector2 nextBgPosition = background.position;
        nextBgPosition.x -= currentVelocity;

        background.position = nextBgPosition;

        clouds.position -= new Vector3(currentVelocity / 6.0f, 0);
    }

    private Coroutine enterOnDecelerationCoroutine;

    public void EnterOnDecelerationState()
    {
        StopAllCoroutines();
        enterOnDecelerationCoroutine = StartCoroutine(EnterOnDecelerationStateCO());
    }

    private IEnumerator EnterOnDecelerationStateCO()
    {
        // DECELERATE
        float step = 0;

        float startValue = currentVelocity;
        float targetValue = data.deceleratePercent/100.0f * data.maxVelocity;

        float seconds = data.decelerationSeconds;

        Debug.Log("Deceleration from " + startValue + " to " + targetValue + " in " + seconds + " seconds.\n");

        while (step < 1)
        {
            currentVelocity = Mathf.Lerp(startValue, targetValue, step);
            step += Time.deltaTime / seconds;
            yield return null;
        }

        currentVelocity = targetValue;

        // BECOME NIGHT
        yield return WeatherManager.Instance.BecomeNight();

        step = 0;
        startValue = currentVelocity;

        while (step < 1)
        {
            step += Time.deltaTime / 1.5f;
            currentVelocity = Mathf.Lerp(startValue, 0, step);
            yield return null;
        }

        // STOP DOG
        canMove = false;
        Player.Instance.SetIdleState();

        // PLAYER PRESS SPACE AND WOOF
        Player.Instance.AvoidJumpAndAllowWoof();

        yield return new WaitForSeconds(.5f);

        // LOOP WOOF
        while (true)
        {
            yield return new WaitForSeconds(10f);

            Player.Instance.Woof();

            yield return null;
        }
    }

    public void StopLogic()
    {
        if (enterOnDecelerationCoroutine != null)
        {
            StopCoroutine(enterOnDecelerationCoroutine);
            enterOnDecelerationCoroutine = null;
        }
    }
}