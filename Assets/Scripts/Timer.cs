﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Timer : Singleton<Timer>
{
    public Text timerText;

    private bool isActive;
    private float timer;

    public void StartTimer()
    {
        isActive = true;
        StartCoroutine(StartTimerCO());
    }

    public void StopTimer()
    {
        isActive = false;
        //Player.Instance.StopPlaySounds();
    }

    private IEnumerator StartTimerCO()
    {
        int minutes, seconds;

        while (isActive)
        {
            timer += Time.deltaTime;

            minutes = Mathf.FloorToInt(timer / 60f);
            seconds = Mathf.FloorToInt(timer - minutes * 60f);

            timerText.text = $"{minutes:00} : {seconds:00}";

            yield return null;
        }
    }
}