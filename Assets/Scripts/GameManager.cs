﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public Camera myCamera;
    public GameDataSO gameData;

    private GameState state;

    public float secondsToStart = 0.5f;

    public enum GameState
    {
        Intro,
        Obstacles,
        Signals,
        NoObstacles,
        Decelerate,
        Night,
        Stop,
        Woof,
        Final
    }

    private IEnumerator Start()
    {
        SoundManager.Instance.PlayMusic("gameplayMusic");

        yield return new WaitForSeconds(secondsToStart);

        StartGameplay();
    }

    internal static void changeStateTo(GameState newState) {
        switch (newState) {
            case GameState.Decelerate:
                
                BackgroundManager.Instance.EnterOnDecelerationState();

                Debug.Log("State: Decelerate");
                break;
            case GameState.Night:
                Debug.LogError("State: Night??????");
                break;
            case GameState.Stop:
                Debug.LogError("State: Stop?????");
                break;
        }
    }

    public void PlayerHitObstacle()
    {
        Debug.Log("Player Collision!\n");

        BackgroundManager.Instance.Decelerate();
        Player.Instance.GetComponentInChildren<Animator>().SetTrigger("Impact");
    }

    public void StartGameplay()
    {
        //Player.Instance.StartPlaySounds();
        Timer.Instance.StartTimer();
        BackgroundManager.Instance.StartMovement();
        Player.Instance.AllowJump();
    }

    public Coroutine StopGameplay()
    {
        return StartCoroutine(StopGameplayCO());
    }

    private IEnumerator StopGameplayCO()
    {
        Player.Instance.AvoidJump();
        state = GameState.Final;

        Timer.Instance.StopTimer();

        WeatherManager.Instance.StopLogic();
        BackgroundManager.Instance.StopLogic();

        yield return BackgroundManager.Instance.Decelerate(true);

        //Debug.Log("HAPPY ENDING");

        SoundManager.Instance.PlayMusic("endMusic");

        Fader.Instance.StartFade(() => SceneManager.LoadScene("Final"));
    }
}