﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
    public void playCuteRun() {
        SoundManager.Instance.PlayEffect("cuteRun", 0.10f);
    }

    public void playWoof() {
        SoundManager.Instance.PlayEffect("cuteBau", 0.3f);
    }

    public void playSadWoof() {
        SoundManager.Instance.PlayEffect("sadBau", 0.3f);
    }
}
