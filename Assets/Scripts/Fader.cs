﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Fader : Singleton<Fader>
{
    public Image mySprite;

    public float fadeInSeconds;
    public float waitSeconds;
    public float fadeOutSeconds;

    private Coroutine fadeCoroutine;

    public Coroutine StartFade(Action action = null)
    {
        if (fadeCoroutine != null)
        {
            return fadeCoroutine;
        }

        return fadeCoroutine = StartCoroutine(StartFadeCO(action));
    }

    private IEnumerator StartFadeCO(Action action)
    {
        if (mySprite == null)
        {
            Debug.LogError("Fader sprite is null!\n");
            yield break;
        }

        float step = 0;
        Color tempColor = mySprite.color;

        if (tempColor.a < 0.95f)
        {
            while (step < 1)
            {
                step += Time.deltaTime / fadeInSeconds;
                tempColor.a = Mathf.Lerp(0, 1, step);
                mySprite.color = tempColor;
                yield return null;
            }
        }

        step = 1;

        action?.Invoke();

        if (Math.Abs(waitSeconds) > 0.05f)
        {
            yield return new WaitForSeconds(waitSeconds);
        }

        while (step > 0)
        {
            step -= Time.deltaTime / fadeOutSeconds;
            tempColor.a = Mathf.Lerp(0, 1, step);
            mySprite.color = tempColor;
            yield return null;
        }

        fadeCoroutine = null;
    }
}